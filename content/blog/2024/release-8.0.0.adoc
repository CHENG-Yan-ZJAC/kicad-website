+++
title = "Version 8.0.0 Released"
date = "2024-02-23"
draft = false
"blog/categories" = [
    "Release Notes"
]
authors = [
    "craftyjon"
]
+++

The KiCad project is proud to announce the release of version 8.0.0.
Despite coming only a year after KiCad 7 was released, version 8 is packed
with new features, improvements, and hundreds of bug fixes. We hope you
enjoy the new version! Head to the https://www.kicad.org/download/[KiCad download page]
to get your copy of the new version (note that some packages may still be in
the process of being released at the time this announcement is published).
Our thanks go out to everyone who contributed to KiCad this past year.  Whether
your contribution was large or small, writing code, submitting bug reports,
improving our libraries and documentation, or just supporting us financially:
your help made a difference.

In accordance with the KiCad stable release policy, KiCad 7.x will no longer be
actively maintained, and we will release bug fix versions of KiCad 8.x over the
next year as we develop new features for KiCad 9. Read on to see some of the
highlights of the new version!

<!--more-->

== Version 8 Changes

Many of the new features in KiCad 8 are described in a
https://forum.kicad.info/t/post-v7-new-features-and-development-news/40144[running thread on the forum],
and Wayne Stambaugh also presented them in his FOSDEM 2024 talk which is
https://www.youtube.com/watch?v=SnEL6TOusoQ[available to watch on YouTube].  This post
highlights some of the changes, but is not a complete list.  There were also
hundreds of bug fixes, performance improvements, and other smaller changes since
KiCad 7.  A full list of issues addressed in KiCad 8 can be found on the
https://gitlab.com/groups/kicad/-/milestones/19#tab-issues[milestone page] on GitLab.

By the numbers:  KiCad 8 was built with changes and additions from hundreds of
developers, translators, and library contributors.  Approximately 4,300 commits
were made that changed code or translations, representing a modest decrease from
Version 7.  This represents a stabilization of the development cycle as we got
through a backlog of new features that had been developed during the long Version 6
cycle and settle in on an annual release pace.  The project continues to bring in
new contributors: this year we had over 70 first-time contributors to the code and
translations.

=== General

==== Improvements to the official KiCad libraries

2023 was a big year for the KiCad library team.  With the help of 8 new librarians joining the
effort, the library gained over 1500 new symbols and 760 footprints, and had thousands of updates
to fix or improve existing parts.  The team processed over 1700 merge requests, about 25% of which
were done in the last month alone!  With many more people helping this year, the team is able to
address issues more quickly and has been driving down the backlog of pending merge requests.  We
are excited to see so many contributors join the library team and accelerate the library's growth.

A common piece of feedback about the KiCad footprint libraries has been that the Pin 1 indication
style, which was based on the ill-fated draft standard IPC-7351C, is not intuitive.  John Beard and
Carsten Presser tackled this for KiCad 8, developing a new standard style and updating our
footprint generators to apply this style to a number of automatically-generated footprints for
SMD packages.  While it will take time to update all footprints to this style, the new style can be
seen on hundreds of footprints already.

image::/img/blog/2024/release-8.0.0/footprints.png[align=center, width=50%, title="New Pin 1 indicators are more clear"]

==== Expanded support for importing data from other tools

KiCad continues to add support for importing and migrating data from other tools.
KiCad 8 adds the ability to import full projects and libraries from EasyEDA (JLCEDA)
Standard and Pro editions _(Alex Shvartzkop)_, CADSTAR symbol and footprint libraries
_(Roberto Fernandez Bautista)_, Solidworks PCB board files _(Jon Evans)_, Altium
Designer symbol and integrated libraries _(Alex Shvartzkop)_, EAGLE symbol libraries
_(Alex Shvartzkop)_, and LTSpice schematics _(Chetan Shinde, Jeff Young, Alex Shvartzkop)_.

The schematic and symbol editors gained the ability to import vector graphics (DXF and SVG),
giving you more options for annotating your designs:

image::/img/blog/2024/release-8.0.0/schematic-vectors.png[align=center, width=50%, title="Vector graphics in the schematic editor"]

The PCB editor also got a new shape healing feature, which fixes up small gaps between
line segments that can be present in drawings imported from other CAD tools.  This
makes it easier to import board outlines, zones, and other complex shapes from
mechanical CAD tools.  Both of these features were contributed by Alex Shvartzkop.

==== New possibilities for data export

In addition to new ways of getting data into KiCad, version 8 brings new ways to get data out.
KiCad's PCB editor now supports exporting boards to IPC-2581 format _(Seth Hillbrand)_, an
all-in-one data package that integrates the fabrication and assembly data for a board.  The STEP
exporter now has an option to export copper shapes along with the rest of the board geometry, for
accurate electromagnetic simulation and other applications _(Jean-Pierre Charras, Alex
Shvartzkop)_.  Last but not least, the schematic editor now supports exporting netlists in Cadence
Allegro format _(Youbao Zhang, David Schneider)_, supporting Allegro users who want to use KiCad as
a schematic capture tool.

==== Improvements to the command-line interface (CLI)

The KiCad CLI was initially launched with version 7, and has already seen a lot of use.
With version 8, the CLI has seen a number of additions from Mark Roszko, Mike Williams, and other
developers:

- DRC and ERC can be run from the CLI, and reports can be generated in a machine-readable
  format (JSON) for use in CI pipelines and other similar applications
- Bills of Material (BOMs) can be exported from the CLI
- glTF and VRML 3D models can be exported from the CLI
- The order of layers can be controlled in multi-layer board plots
- The drawing sheet can be overridden when plotting from the command line
- Text variables can be overridden in the command line (for example, to customize the value of
  a text variable as part of a CI pipeline)
- A number of existing options gained new control flags and several bugs were fixed

==== Improvements to documentation

Graham Keeth has been hard at work updating KiCad's documentation for Version 8. Thanks to his
work, many of the new features and changes in KiCad 8 are already documented (which long-time users
will know is a much better situation than in previous releases).  In addition to documenting new
features, Graham has been improving all areas of the documentation with updated screenshots,
copy editing, and corrections.  Our documentation translators have also been working to update the
translated text so that up-to-date KiCad documentation is available in more languages.

==== Other smaller KiCad-wide changes

- An alternate (second) hotkey can be assigned to any action
- The library editors now show a small preview of symbols and footprints when hovering over their
  names in the library lists

=== Schematics and Symbols

==== New UI panels for properties, net navigation, and search

The schematic editor in KiCad 8 features several new UI panels that can be shown and hidden via the
View menu.  The properties panel _(Jon Evans)_, similar to the one found in the PCB editor in KiCad
7, allows fast editing of the properties of selected items (and is also now available in the symbol
editor).  The search panel _(Mark Roszko)_ allows quick access to search results across large
schematics.  The net navigator _(Wayne Stambaugh)_ shows the path a highlighted net takes across a
complex hierarchical design.

image::/img/blog/2024/release-8.0.0/schematic-ui.png[align=center, width=75%, title="New UI in KiCad 8's schematic editor"]

==== New BOM exporter

In the past, customizing the data and formatting of a bill of materials exported from KiCad
required the use of external tools such as Python scripts.  For version 8, Mike Williams developed
a brand-new BOM exporter built-in to KiCad, allowing you to select and reorder columns and control
the formatting of exported BOMs from within the symbol fields table dialog.  Once you are happy
with an export style, save it as a preset for easy recall.

image::/img/blog/2024/release-8.0.0/bom-export.png[align=center, width=75%, title="Customize and export a BOM from the new graphical exporter UI"]

==== Pin helpers

A new set of contextual tools in the schematic editor were added by Mike Williams: Pin Helpers
allow you to create labels, wires, and no-connect markers from symbol pins in just a few clicks.

++++
<div class="row justify-content-center">
    <div class="ratio ratio-16x9 w-75">
        <video controls muted playsinline class="embed-responsive-item">
            <source src="/video_clips/blog/2024/release-8.0.0/pin-helpers.mp4" type="video/mp4">
        </video>
    </div>
</div>
++++

==== Comparison of symbols to library versions

KiCad has been able to report that a symbol is different from its library version for a while (for
example, due to you editing the symbol locally on the schematic, or updating the library version
after placing the symbol on a schematic) but it is sometimes hard to tell what is different.  To
improve this situation, Jeff Young developed a comparison tool for symbols and footprints that
shows a report and visual comparison between the schematic and library versions of a selected
symbol:

image::/img/blog/2024/release-8.0.0/compare-symbol.png[align=center, width=50%, title="The Compare Symbol with Library dialog shows a visual \"onion-skin\" comparison"]

==== Improvements to grid handling

Mike Williams and Jeff Young improved the grid preferences in the schematic editor and introduced
grid overrides: a feature that forces a certain grid when performing operations (moving, placing,
etc) on a certain type of object.  With this feature enabled, you can switch between different
grids for placing text, but always keep symbols and wires on a 50 mil grid to match with the
symbol library pin spacing.

image::/img/blog/2024/release-8.0.0/schematic-grids.png[align=center, width=75%, title="New grid preferences make it easier to keep connected items on the same grid"]


==== Editable power symbols

A long-standing difference between KiCad and some other EDA tools has been that KiCad's power
symbols (single-pin symbols with names such as `VCC` and `GND`) create net names based on their
pin names, which can only be renamed in the symbol library editor, not after they have been placed
on a schematic.  For KiCad 8, Mike Williams removed this limitation, by making power symbols take
their net name from their Value field rather than their pin name.  This means that custom voltage
rails can be created without creating additional symbols.

==== Other smaller changes to the schematic editor

- Named variable fields: there are some special names that can be used for custom fields such as
  `${DNP}` which will correspond to a certain symbol property (in this case, Do Not Place).  These
  can be used to export these properties along with other fields in a bill of materials.
- An automatic Item Number column has been added to the Symbol Fields Table / BOM Exporter
- Alternate pin functions can now be selected from the context menu
- Arcs greater than 180 degrees can be created and edited

=== Simulation

KiCad 8 brings a number of improvements to the embedded SPICE simulation tools, which are powered
by https://ngspice.sourceforge.io/[ngspice].  The developers of both projects have been working
together over the past year to add new features and fix bugs, making KiCad a practical choice for
circuit simulation for many more users.

==== Simulator UI overhaul

Jeff Young overhauled the user interface of the simulator for KiCad 8.  In addition to having a
more polished look and feel, the new simulator makes it possible to manage plots of many signals,
take measurements with cursors and analytical functions, and plot power signals in addition to
voltage and current.

image::/img/blog/2024/release-8.0.0/simulator.jpg[align=center, width=75%, title="New UI in KiCad 8's SPICE simulator"]

==== New simulation features

The changes are not just to the user interface, though: there are four new simulation types that
can be configured (pole-zero, noise, S-parameter, and FFT).  Custom signals can be defined, meaning
plots can be added for expressions such as `V(/in) - V(/out)`, making it possible to visualize many
more outputs of a simulation from within KiCad.  Along these lines, operating point simulation
results can now be visualized directly on the schematic canvas _(Jeff Young)_:

image::/img/blog/2024/release-8.0.0/operating-points.png[align=center, width=75%, title="Operating points visible on a schematic"]

=== PCBs and Footprints

==== Multiple footprint dragging

KiCad 7 introduced a limited ability to drag footprints with tracks attached.  In KiCad 8, this
has been extended to allow dragging of more than one footprint at once _(Jon Evans)_:

++++
<div class="row justify-content-center">
    <div class="ratio ratio-16x9 w-75">
        <video controls muted playsinline class="embed-responsive-item">
            <source src="/video_clips/blog/2024/release-8.0.0/multi-drag.mp4" type="video/mp4">
        </video>
    </div>
</div>
++++

==== Interactive length tuning patterns

Alex Shvartzkop gave KiCad's length tuning pattern tools a complete overhaul for version 8,
and now tuning patterns are objects that can be selected, modified, and removed after they are
initially placed.  This greatly speeds up the process of modifying designs with controlled-length
routing.

++++
<div class="row justify-content-center">
    <div class="ratio ratio-16x9 w-75">
        <video controls muted playsinline class="embed-responsive-item">
            <source src="/video_clips/blog/2024/release-8.0.0/meanders.mp4" type="video/mp4">
        </video>
    </div>
</div>
++++

==== Connectivity for graphic shapes

KiCad has historically drawn a distinction between electrically-connected objects such as pads,
tracks, vias, and zones, and ordinary graphical shapes such as lines and polygons.  The latter
could be drawn on any PCB layer, but could not be assigned to a net without creating a footprint
and using custom shape pads.  For KiCad 8, Jon Evans removed this restriction, meaning that
designers can draw complex copper geometry directly within the board editor, and import copper
shapes from external CAD tools for applications such as RF, coil, and sensor design.

image::/img/blog/2024/release-8.0.0/shapes.png[align=center, width=75%, title="Graphic shapes assigned to a net"]

==== Footprint editor properties panel

The properties panel, first introduced in the PCB editor in KiCad 7, has been expanded to the
library editors in KiCad 8 by Jon Evans.  This panel, which can be shown and hidden from the View
menu, allows quick access to many properties of selected objects.  Using it allows more efficient
creation and modification of footprints, as things such as pad size and location can be changed
with fewer clicks.  As with many other places in KiCad, numeric fields in the properties panel
support simple mathematical expression evaluation, meaning you can do things such as add an offset
to a pad's position without doing the math in your head.

image::/img/blog/2024/release-8.0.0/footprint-editor.png[align=center, width=75%,title="The properties panel allows quick editing of footprints"]

==== 3D Viewer UI overhaul

Jeff Young reworked the user interface of the 3D viewer to feel more like the rest of the PCB
editor.  It now features an appearance panel, visibility presets, and viewports (saved camera
positions).  It is now much easier to control the visibility of different types of parts and
layers of the board.

image::/img/blog/2024/release-8.0.0/3d-viewer.png[align=center, width=75%,title="The 3D viewer now has easy-to-use view controls"]

==== Other smaller changes to the PCB editor

- Footprints can be compared with their library versions, much like the symbol version described above
- Footprints now have fields, synchronized with their corresponding symbol's fields
- The thickness of ratsnest lines can be customized
- Board-level text and graphics settings can optionally override those used in a footprint library
- Expand Selection (hotkey `U`) now works on graphic shapes in addition to copper objects
- Custom-shape pads can now have custom thermal spoke templates defined
