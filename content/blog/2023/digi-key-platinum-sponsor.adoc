+++
title = "Digi-Key Electronics Becomes Platinum Sponsor"
date = "2023-01-27"
draft = true
"blog/categories" = [
    "News"
]
+++

The KiCad project is excited to announce that https://www.digikey.com/[Digi-Key Electronics]
has become our latest platinum sponsor.  Digi-Key Electronics has been a long time sponsor of
the project and is responsible for securing the kicad.org domain name.  Their donation during
this years funding campaign has moved them to the platinum donation level.  The KiCad project
is grateful for their support and we look forward to our continued partnership with Digi-Key
Electronics.
