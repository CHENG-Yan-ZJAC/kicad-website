/*!
 * KiCad js
 */

(function() {

    if ($('.navbar').hasClass('transparent')) {
        $(window).scroll(function() {
            if ($(this).scrollTop() < 430) {
                $('.navbar').addClass('transparent');
            } else {
                $('.navbar').removeClass('transparent');
            }
        });
    }

    $(window).resize(function() {
        var mobileLimit = 760;
        if ($(window).width() < mobileLimit) {
            $('.navbar').addClass('navbar-static-top');
            $('.navbar').removeClass('navbar-fixed-top');
            $('header').addClass('page-header-decor-no-margin');
        } else {
            $('.navbar').removeClass('navbar-static-top');
            $('.navbar').addClass('navbar-fixed-top');
            $('header').removeClass('page-header-decor-no-margin');
        }
    });
    $(window).trigger('resize');

    // Autoplay videos only if taking up a chunk of the display
    $(window).scroll(function() {
        var display_threshold = 0.8;    // fraction of video that must be displayed to trigger autoplay
        $('video').each(function(){
            var videoHeight = $(this).height();
            var videoWidth = $(this).width();
            var videoOffset = $(this).offset();

            var x = videoOffset.left,
                y = videoOffset.top,
                w = videoWidth,
                h = videoHeight,
                r = x + w, //right
                b = y + h; //bottom

            var visibleX = Math.max(0, Math.min(w, window.pageXOffset + window.innerWidth - x, r - window.pageXOffset));
            var visibleY = Math.max(0, Math.min(h, window.pageYOffset + window.innerHeight - y, b - window.pageYOffset));

            var visible = visibleX * visibleY / (w * h);

            if (visible > display_threshold) {
                $(this)[0].play();
            } else {
                $(this)[0].pause();
            }
        })
    })

})();

/*
 * Add Donate thank you page during download
 */
$(".dl-link").click(function() {
    $(".initial-text").hide();
    $(".page-heading").hide();
    $(".donate-hidden").show();
    $(".backup-link")[0].href = this.href;
})

$(".osdn").click(function() {
    var i = document.createElement('iframe');
    i.style.display = 'none';
    var uri = parseUri(this.href);
    i.src = 'https://osdn.net/frs/redir.php?f=/storage/g/k/ki/kicad/' + uri.file;
    document.body.appendChild(i);
    return false;
})

// parseUri 1.2.2
// (c) Steven Levithan <stevenlevithan.com>
// MIT License

function parseUri(str) {
    var o = parseUri.options,
        m = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
        uri = {},
        i = 14;

    while (i--) uri[o.key[i]] = m[i] || "";

    uri[o.q.name] = {};
    uri[o.key[12]].replace(o.q.parser, function($0, $1, $2) {
        if ($1) uri[o.q.name][$1] = $2;
    });

    return uri;
};

parseUri.options = {
    strictMode: false,
    key: ["source", "protocol", "authority", "userInfo", "user", "password", "host", "port", "relative", "path", "directory", "file", "query", "anchor"],
    q: {
        name: "queryKey",
        parser: /(?:^|&)([^&=]*)=?([^&]*)/g
    },
    parser: {
        strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
    }
};

// https://www.quirksmode.org/js/cookies.html
function setCookie(name, value, days) {
    var expires = "";
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/; SameSite=None; Secure";
  }

  function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
  }