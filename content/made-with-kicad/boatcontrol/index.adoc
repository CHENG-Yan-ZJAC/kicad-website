+++
title = "Boatcontrol"
projectdeveloper = "Antonia Stevens (ant)"
projecturl = "https://github.com/antevens/boatcontrol"
"made-with-kicad/categories" = [
    "Pi Hat"
]
+++

Boatcontrol is an open hardware design for a marine grade SMART/IoT electrical
distribution panel and electronics platform (boat) controller.

It's an I/O breakout board capable of switching 32 onboard latching relays and
optionally 32 momentary/latching relays using addon boards switching up to 60A
AC/DC 0-400V per circuit.

The "smart" part is provided by either a **Raspberry Pi** HAT or a
**Nvidia Jetson** (Nano/Xavier NX) either of which can be mounted on the board.
