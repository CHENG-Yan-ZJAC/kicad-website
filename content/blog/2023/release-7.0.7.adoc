+++
title = "KiCad 7.0.7 Release"
date = "2023-08-15"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the fifth series 7 bug fix release.
The 7.0.7 stable version contains critical bug fixes and other minor improvements
since the previous release.

<!--more-->

A list of all of the fixed issues since the 7.0.6 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/29[KiCad 7.0.7
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 7.0.7 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/7.0/[7.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- Fix build link error when linking nanoodbc. https://gitlab.com/kicad/code/kicad/-/issues/15081[#15081]
- Fix broken text shadow offsets. https://gitlab.com/kicad/code/kicad/-/issues/15019[#15019]
- https://gitlab.com/kicad/code/kicad/-/commit/0ffef66cb68a5178dca9d9d165dfa8a423e60312[Fix bitmap transparency in Cairo GAL].
- Treat undo as backspace and/or escape when drawing polygons. https://gitlab.com/kicad/code/kicad/-/issues/14961[#14961]
- Handle some macOS standard keyboard shortcuts in custom text editors. https://gitlab.com/kicad/code/kicad/-/issues/14973[#14973]
- Fix text variable auto completion with return key. https://gitlab.com/kicad/code/kicad/-/issues/15001[#15001]
- https://gitlab.com/kicad/code/kicad/-/commit/810f3efcbb434980da6dd7f299512f1bbc702be1[Always show the footprint in the default orientation in the 3D viewer panel].

=== Schematic Editor
- https://gitlab.com/kicad/code/kicad/-/commit/dfc17244d49e04985c9c60bc6435715fcfe2d1dd[Improve search performance].
- https://gitlab.com/kicad/code/kicad/-/commit/70783811756240bd9f53a284b506b1217e75ad67[Fix printing background fill when paper orientation or size differs].
- https://gitlab.com/kicad/code/kicad/-/commit/6f384c880203dac7363ca59888aa3ca10320eda1[Import Altium net label justification].
- Do not arbitrarily zoom when canceling page settings dialog. https://gitlab.com/kicad/code/kicad/-/issues/15033[#15033]
- Fix slow warping and zoom reset when searching with find dialog. https://gitlab.com/kicad/code/kicad/-/issues/14305[#14305]
- Create correct new wire color when dragging net labels. https://gitlab.com/kicad/code/kicad/-/issues/15178[#15178]
- Fix incorrect color when continuing a wire. https://gitlab.com/kicad/code/kicad/-/issues/15192[#15192]
- Do not ignore custom fields when updating from text and graphics properties dialog. https://gitlab.com/kicad/code/kicad/-/issues/15206[#15206]
- Respect ERC error indicator color setting. https://gitlab.com/kicad/code/kicad/-/issues/15203[#15203]
- Fix crash when loading schematic with missing sub-sheet files. https://gitlab.com/kicad/code/kicad/-/issues/15222[#15222]
- Sort bus aliases by name to prevent VCS issues. https://gitlab.com/kicad/code/kicad/-/issues/11890[#11890]
- Ensure the bitmap exported to the clipboard is valid. https://gitlab.com/kicad/code/kicad/-/issues/14808[#14808]
- Fix broken cancel button in export symbols to new library dialog. https://gitlab.com/kicad/code/kicad/-/issues/15278[#15278]
- Preserve unit associations when re-annotating. https://gitlab.com/kicad/code/kicad/-/issues/14918[#14918]
- https://gitlab.com/kicad/code/kicad/-/commit/56aff41b7ee254a88e9773cb66e99a9c9edec9e2[Database libraries performance improvements].
- Remove unwanted outline in exported graphics. https://gitlab.com/kicad/code/kicad/-/issues/15242[#15242]
- Fix crash when power symbol is connected to hierarchical bus. https://gitlab.com/kicad/code/kicad/-/issues/15217[#15217]
- Fix crash when using '/' character in sheet name. https://gitlab.com/kicad/code/kicad/-/issues/15367[#15367]

=== Footprint Assignment Tool
- Fix crash when attempting to use ratsnest settings. https://gitlab.com/kicad/code/kicad/-/issues/15153[#15153]

=== Board Editor
- https://gitlab.com/kicad/code/kicad/-/commit/cadea901091bb8d44ebbf7dd389f0e967c13e0c1[Check for zone to zone overlap].
- https://gitlab.com/kicad/code/kicad/-/commit/bbf59a731d333fd4245b6b579f7b383bbf92d0e3[Fix EAGLE import footprint text alignment].
- Fix unconnected track end warning for track stubs inside vias and pads. https://gitlab.com/kicad/code/kicad/-/issues/14211[#14211]
- Fix crash interpreting malformed rule. https://gitlab.com/kicad/code/kicad/-/issues/14989[#14989]
- Fix DRC bug created by broken rounded rule zone fill. https://gitlab.com/kicad/code/kicad/-/issues/15007[#15007]
- Fix crash when selecting grouped items. https://gitlab.com/kicad/code/kicad/-/issues/15021[#15021]
- Save project when saving the board. https://gitlab.com/kicad/code/kicad/-/issues/14991[#14991]
- https://gitlab.com/kicad/code/kicad/-/commit/16d24d4b431c5715b2ee790dd5a36bf893e089b9[Tailor object inspector text properties for dimension objects].
- Allow routing to a free pad when it has a hole. https://gitlab.com/kicad/code/kicad/-/issues/15093[#15093]
- Add SHORT_NET_NAME processing to footprint variable resolution. https://gitlab.com/kicad/code/kicad/-/issues/15095[#15095]
- Fix crash when opening zone properties dialog or object properties panel. https://gitlab.com/kicad/code/kicad/-/issues/12871[#12871]
- Fix shorts created by zone fill. https://gitlab.com/kicad/code/kicad/-/issues/15160[#15160]
- Fix plotting performance issues when text is on mask layers. https://gitlab.com/kicad/code/kicad/-/issues/15161[#15161]
- Make undo of set grid origin restore Y coordinate correctly. https://gitlab.com/kicad/code/kicad/-/issues/15177[#15177]
- Fix DRC error indicator position. https://gitlab.com/kicad/code/kicad/-/issues/15209[#15209]
- https://gitlab.com/kicad/code/kicad/-/commit/45d496bd010e05812d3bc8efeb3876ff3f219410[Fix Altium importer orientation of polygons and non copper pads in footprints].
- Fix broken net-tie DRC. https://gitlab.com/kicad/code/kicad/-/issues/14008[#14008]
- Do not ignore trace routing clearances. https://gitlab.com/kicad/code/kicad/-/issues/15162[#15162]
- https://gitlab.com/kicad/code/kicad/-/commit/49772d2539cb2a04d6b786f40337cdcf832fa8bb[Fix incorrect GERBER X3 key word].
- https://gitlab.com/kicad/code/kicad/-/issues/15288[Prevent copper finish setting in board setup dialog from  getting reset to none].
- Fix layer preset object visibility regression. https://gitlab.com/kicad/code/kicad/-/issues/15282[#15282]
- Do not ignore NPTHs when dragging via in shove mode. https://gitlab.com/kicad/code/kicad/-/issues/15117[#15117]
- Allow shoving tracks with attached via on layers other than the top and bottom. https://gitlab.com/kicad/code/kicad/-/issues/12873[#12873]
- Fix loading of via drill default sizes and other settings from legacy board files. https://gitlab.com/kicad/code/kicad/-/issues/15350[#15350]
- https://gitlab.com/kicad/code/kicad/-/commit/5b73af668f056ac780996d6e076c2e85492a42d0[Remove invalid polygons],
https://gitlab.com/kicad/code/kicad/-/commit/8e1466a35a1c8173ead90f5475425412d34e8f8e[ import holes in non-copper polygons],
https://gitlab.com/kicad/code/kicad/-/commit/acc03e91f3d4ca5d3d4bd946fdf78d76065ceef4[ import solder paste/mask expansion rules],
https://gitlab.com/kicad/code/kicad/-/commit/2eb6ca75a813f273d23ac037daf7df0601661890[ import dashed outlines], and
https://gitlab.com/kicad/code/kicad/-/commit/4918ac63072e83f5a52a29576856be2fbfa17339[other improvements] to the Altium importer.
- https://gitlab.com/kicad/code/kicad/-/commit/75ace76c6ab8e9132822d0aeda9d0137187f67e9[Minor router and DRC performance improvements].

=== Footprint Editor
- Update new footprint library save path when changing projects. https://gitlab.com/kicad/code/kicad/-/issues/15102[#15102]
- Fix crash when moving a solder paste aperture relative to an existing pad. https://gitlab.com/kicad/code/kicad/-/issues/15090[#15090]
- Prevent crash when typing '${' in text control. https://gitlab.com/kicad/code/kicad/-/issues/15283[#15283]

=== 3D Viewer
- Improve calculation of min and max zoom. https://gitlab.com/kicad/code/kicad/-/issues/15078[#15078]

=== Drawing Sheet Editor
- Fix saving text with custom font. https://gitlab.com/kicad/code/kicad/-/issues/15154[#15154]

=== Windows
- https://gitlab.com/kicad/code/kicad/-/commit/ecb9f607edd90ee3f0d8a6efde91e5206633ca47[Move kicad-vcpkg forward to pickup new version of OpenCascade].
- Fix 3D model viewer panel height to prevent clipped controls. https://gitlab.com/kicad/code/kicad/-/issues/13896[#13896]
