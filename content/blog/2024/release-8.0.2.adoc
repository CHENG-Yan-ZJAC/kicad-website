+++
title = "KiCad 8.0.2 Release"
date = "2024-04-28"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the version 8.0.2 bug fix release.
The 8.0.2 stable version contains critical bug fixes and other minor
improvements since the previous release.

<!--more-->

A list of all of the fixed issues since the 8.0.1 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/36[KiCad 8.0.2
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 8.0.2 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/8.0/[8.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- Correctly resolve variable ${CURRENT_DATE} in title block. https://gitlab.com/kicad/code/kicad/-/issues/17256[#17256]
- Correct too thin over bar when exporting to PDF with non-default font. https://gitlab.com/kicad/code/kicad/-/issues/17218[17218]
- Add environment variable for configuring stock data home path. https://gitlab.com/kicad/code/kicad/-/issues/15687[#15687]
- Fix a library tree pane search issues. https://gitlab.com/kicad/code/kicad/-/issues/17205[#17205]
- Fix rendering/plotting of arcs with tiny angle and huge radius. https://gitlab.com/kicad/code/kicad/-/issues/17343[#17343]
- https://gitlab.com/kicad/code/kicad/-/commit/c07604bb0332101c9abb453b723ef50d9db434cc[Add autocomplete accept and cancel to the hotkeys list].
- https://gitlab.com/kicad/code/kicad/-/commit/11177bf65857e14cff4be0f01aa303bc7322650d[Fix selected item count in the properties panel].
- Add newline to end of file. https://gitlab.com/kicad/code/kicad/-/issues/17480[#17480]
- Reduce the number of displayed digits for coordinate edit controls. https://gitlab.com/kicad/code/kicad/-/issues/15539[#15539]
- Fix Arial font drawing artifacts. https://gitlab.com/kicad/code/kicad/-/issues/17621[#17621]
- Add missing file icons. https://gitlab.com/kicad/code/kicad/-/issues/17525[#17525]
- Add libgit2 version 1.8.0 compatibility. https://gitlab.com/kicad/code/kicad/-/issues/17536[#17536]
- Fix crash in configure paths using non-ascii characters in column headers. https://gitlab.com/kicad/code/kicad/-/issues/17743[#17743]


=== Schematic Editor
- Allow the simulation DC operating point labels to be repositioned. https://gitlab.com/kicad/code/kicad/-/issues/17228[#17228]
- Show pin names in footprint viewer after assigned to symbol. https://gitlab.com/kicad/code/kicad/-/issues/17349[#17349]
- Always define default net class even when it doesn't exist. https://gitlab.com/kicad/code/kicad/-/issues/17388[#17388]
- https://gitlab.com/kicad/code/kicad/-/commit/e196f71d58cdf7b856f745933c09af7e0e3d4953[Support importing Altium ASCII schematic files].
- https://gitlab.com/kicad/code/kicad/-/commit/c1a21c9ab1cf6d7e1cd66db88560b87d19458e49[Place items on root sheet when it's the only sheet when importing EasyEDA schematic].
- https://gitlab.com/kicad/code/kicad/-/commit/10ca34e2a7f47d91d06d5311de4e5b6d63f62a85[Support dot and clock symbol pin styles when importing EasyEDA schematic].
- Support multiple sheet schematics on EasyEDA import. https://gitlab.com/kicad/code/kicad/-/issues/17219[#17219]
- Fix broken paste special keep existing symbol annotations. https://gitlab.com/kicad/code/kicad/-/issues/17231[#17231]
- https://gitlab.com/kicad/code/kicad/-/commit/e904731e2807d6d13b2f82ef6c201bfa1757529d[Do not update schematic connectivity for irrelevant property changes].
- Fix database library cache memory leak. https://gitlab.com/kicad/code/kicad/-/issues/17435[#17435]
- Do not tab focus on random symbol when cross probing. https://gitlab.com/kicad/code/kicad/-/issues/17531[#17531]
- https://gitlab.com/kicad/code/kicad/-/commit/70a6697c169d80fc4f3ccc5e0738cbfc607e4e2a[Fix warning due to unused angle in EasyEDA Pro schematic parser].
- Do not open ERC dialog when clicking ERC marker. https://gitlab.com/kicad/code/kicad/-/issues/17383[#17383]
- Select correct ERC dialog entry when double clicking ERC marker. https://gitlab.com/kicad/code/kicad/-/issues/17383[#17383]
- Check for bus no-connects in ERC. https://gitlab.com/kicad/code/kicad/-/issues/13285[#13285]
- Maintain hierarchy navigator expansion state between edits. https://gitlab.com/kicad/code/kicad/-/issues/16635[#16635]
- Update hierarchy navigator when undoing or redoing sheet name changes. https://gitlab.com/kicad/code/kicad/-/issues/17721[#17721]
- Fix incremental connectivity issue. https://gitlab.com/kicad/code/kicad/-/issues/17528[#17528]
- Make nets with net class directives remember which net class they belong to. https://gitlab.com/kicad/code/kicad/-/issues/17720[#17720]
- Close file when written on BOM export. https://gitlab.com/kicad/code/kicad/-/issues/17779[#17779]
- https://gitlab.com/kicad/code/kicad/-/commit/8d62a5fd3fba86e949fa45c4be37e10e05ebfe3a[Fix missing instance data when reusing an already loaded schematic].
- Fix crash on consecutive symbol changes with different pin counts. https://gitlab.com/kicad/code/kicad/-/issues/17851[#17851]


=== Spice Simulator
- Allow deletion of a user-defined signal. https://gitlab.com/kicad/code/kicad/-/issues/17395[#17395]
- Remove unsaved flag (*) from title bar after saving simulation workbook. https://gitlab.com/kicad/code/kicad/-/issues/17411[#17411]
- Use correct tooltip for tune resistor series buttons. https://gitlab.com/kicad/code/kicad/-/issues/17515[#17515]
- Don't show hidden text in symbol previews. https://gitlab.com/kicad/code/kicad/-/issues/17526[#17526]
- Close active cell editor when showing/hiding columns in symbol fields editor. https://gitlab.com/kicad/code/kicad/-/issues/17425[#17425]
- https://gitlab.com/kicad/code/kicad/-/commit/75ef003747af5eeabc0e4849c0afc5f89e01d2b9[Add option to preserve custom power flag values in "Update Symbols from Library" dialog].


=== Symbol Editor
- Set description on Altium imported symbols. https://gitlab.com/kicad/code/kicad/-/issues/16943[#16943]
- https://gitlab.com/kicad/code/kicad/-/commit/b718c228fe976ed828b9830e2ada8d7768be415e[Do not allow to selection of invisible pins and fields].
- https://gitlab.com/kicad/code/kicad/-/commit/92275c41cc89881051959c15382222b2a5483284[Expose units and alternate body style to properties panel].


=== Board Editor
- Run edge cuts and margin clearance tests on zone layers. https://gitlab.com/kicad/code/kicad/-/issues/17292[#17292]
- Don't allow selection of hidden footprint text in board editor. https://gitlab.com/kicad/code/kicad/-/issues/17271[#17271]
- Make Edit Track & Via Sizes dialog "specified sizes" dropdowns clearer. https://gitlab.com/kicad/code/kicad/-/issues/17216[#17216]
- Remove netclass/custom rule handling from Track & Via Properties. https://gitlab.com/kicad/code/kicad/-/issues/17216[#17216]
- Do not flag blind via's on different layers as "Drilled holes too close together" by DRC. https://gitlab.com/kicad/code/kicad/-/issues/17426[#17426]
- Allow specifying a minimum for a via count constraint. https://gitlab.com/kicad/code/kicad/-/issues/17234[#17234]
- https://gitlab.com/kicad/code/kicad/-/commit/4ac848f18ae45a1aa3eabfdc23355efe8d25a0cf[Fix crash in EasyEDA importer when Name/Prefix type text is not in a footprint].
- Fix position and orientation of footprint graphics on Fabmaster (CADENCE ASCII) import. https://gitlab.com/kicad/code/kicad/-/issues/17239[#17239]
- https://gitlab.com/kicad/code/kicad/-/commit/ed89827908f9e109d2457445f7d2215ed2840794[Fix zone cross-probing from DRC dialog].
- Fix crash when importing Altium PcbDoc. https://gitlab.com/kicad/code/kicad/-/issues/17351[#17351]
- Fix Eagle board import when footprint library versions exist. https://gitlab.com/kicad/code/kicad/-/issues/12897[#12897]
- Improve performance when toggling view of rats nest lines assigned to net classes. https://gitlab.com/kicad/code/kicad/-/issues/17115[#17115]
- Correctly handle locked table cells. https://gitlab.com/kicad/code/kicad/-/issues/17439[#17439]
- Fix when swapping layers. https://gitlab.com/kicad/code/kicad/-/issues/17371[#17371]
- Fix issue for boards with round outline on STEP export. https://gitlab.com/kicad/code/kicad/-/issues/17446[#17446]
- Handle small segments connecting two arcs better. https://gitlab.com/kicad/code/kicad/-/issues/17499[#17499]
- Correctly translate Chinese characters in gerber file output. https://gitlab.com/kicad/code/kicad/-/issues/17534[#17534]
- Fix clearance violation between a filled zone and a net tie polygon. https://gitlab.com/kicad/code/kicad/-/issues/17223[#17223]
- https://gitlab.com/kicad/code/kicad/-/commit/869f30e2915c8b0e799a08af74aa9cb31baabf9c[Add toggle horizontal, vertical, 45 degree mode entry to the measure tool's context menu].
- Fix performance issue when editing length tuning on net inspector highlighted net. https://gitlab.com/kicad/code/kicad/-/issues/17068[#17068]
- Fix excessive hang after a move and undo. https://gitlab.com/kicad/code/kicad/-/issues/17420[#17420] and https://gitlab.com/kicad/code/kicad/-/issues/17561[#17561]
- Sort netnames in properties panel. https://gitlab.com/kicad/code/kicad/-/issues/15590[#15590]
- Fix zone fill pad connection issues. https://gitlab.com/kicad/code/kicad/-/issues/17559[#17559]
- Expose groups to property manager. https://gitlab.com/kicad/code/kicad/-/issues/17496[#17496]
- limiting text height and width to 1 mil. https://gitlab.com/kicad/code/kicad/-/issues/17543[#17543]
- Fix stack overflow when running DRC. https://gitlab.com/kicad/code/kicad/-/issues/17560[#17560]
- Fix ghost footprint reference after moving than undoing. https://gitlab.com/kicad/code/kicad/-/issues/17592[#17592]
- Improve DRC performance due to user interface yields. https://gitlab.com/kicad/code/kicad/-/issues/17434[#17434]
- Do not how negative track length after deleting length tuned track in net inspector. https://gitlab.com/kicad/code/kicad/-/issues/17527[#17527]
- Fix slow selection time when calculating clearance. https://gitlab.com/kicad/code/kicad/-/issues/17327[#17327]
- Fix freeze when selecting two zones on the same layer with different nets. https://gitlab.com/kicad/code/kicad/-/issues/17327[#17327]
- Fix race condition in zone fill. https://gitlab.com/kicad/code/kicad/-/issues/17180[#17180]
- Avoid crashing on missing tracks when importing from CADSTAR. https://gitlab.com/kicad/code/kicad/-/issues/17523[#17523]
- Prevent PNS router from creating tracks with clearance violations. https://gitlab.com/kicad/code/kicad/-/issues/16879[#16879]
- Do not violate DRC rules when dragging via. https://gitlab.com/kicad/code/kicad/-/issues/16293[#16293]
- https://gitlab.com/kicad/code/kicad/-/commit/c0d38de280906d9e86cb0fa785f1fb1d0f69fe63[Add corrections to IPC2581 export syntax].
- Update footprint field when changing footprints. https://gitlab.com/kicad/code/kicad/-/issues/17598[#17598]
- Fix crash when opening board. https://gitlab.com/kicad/code/kicad/-/issues/17664[#17664]
- Prevent tracks from disappearing on move. https://gitlab.com/kicad/code/kicad/-/issues/17110[#17110]
- Fix crash when box selection contains a via and a rule area.  https://gitlab.com/kicad/code/kicad/-/issues/17687[#17687]
- Do not drop segments when rerouting existing track and "remove redundant track" is set. https://gitlab.com/kicad/code/kicad/-/issues/17582[#17582]
- Fix reversed text in flipped footprints. https://gitlab.com/kicad/code/kicad/-/issues/17546[#17546]
- Fix hang after moving footprint. https://gitlab.com/kicad/code/kicad/-/issues/17763[#17763]
- Do not fail when cannot construct a wire on STEP export. https://gitlab.com/kicad/code/kicad/-/issues/17774[#17774]
- Fix misaligned solder mask for chamfered pads in gerber output. https://gitlab.com/kicad/code/kicad/-/issues/17793[#17793]
- Fix zone fill crash with custom design rule with courtyard condition. https://gitlab.com/kicad/code/kicad/-/issues/17791[#17791]


=== Footprint Editor
- Fix importing EasyEDA footprints. https://gitlab.com/kicad/code/kicad/-/issues/17264[#17264]
- https://gitlab.com/kicad/code/kicad/-/commit/8275176792f3913a6abdf286e10a15bd553c2689[Report location of future format errors when reading footprints].
- Fix crash when pressing a number key. https://gitlab.com/kicad/code/kicad/-/issues/17522[#17522]


=== 3D Viewer
- Improve STEP model import precision. https://gitlab.com/kicad/code/kicad/-/issues/17168[#17168]
- https://gitlab.com/kicad/code/kicad/-/commit/663e857df62e229ebf452cdfa6286fc7b29934e9[Fix graphic issues with zero thickness copper].
- Allow setting colors not in board stackup when the board stackup option is set. https://gitlab.com/kicad/code/kicad/-/issues/17632[#17632]
- Fix cryptic error message "input line too long" when importing 3D model fails. https://gitlab.com/kicad/code/kicad/-/issues/17727[#17727]


=== Gerber Viewer
- Show "Edit Grids..." menu entry. https://gitlab.com/kicad/code/kicad/-/issues/17372[#17372]


=== Python scripting
- Fix SWIG wrapper of PAD AddPrimitive() method. https://gitlab.com/kicad/code/kicad/-/issues/17676[#17676]


=== Command Line Interface
- Add missing text variable override when plotting gerbers. https://gitlab.com/kicad/code/kicad/-/issues/17283[#17283]
- Fix crash when processing schematic that references title block variables. https://gitlab.com/kicad/code/kicad/-/issues/17790[#17790]


=== Windows
- https://gitlab.com/kicad/code/kicad/-/commit/dcd304c5d8af2131bec9137724f9e84f5c5fb264[Change the platform detection order].
- Accept hotkey using Alt key modifier. https://gitlab.com/kicad/code/kicad/-/issues/17453[#17453]


=== macOS
- Fix crash when toggling display of 3D models from off to on in 3D viewer. https://gitlab.com/kicad/code/kicad/-/issues/17198[#17198]
- Fix crash on start up. https://gitlab.com/kicad/code/kicad/-/issues/17749[#17749]


=== Linux
- Fix background color opacity with KDE plasma desktop. https://gitlab.com/kicad/code/kicad/-/issues/17341[#17341]
- Disable infinite panning when using XWayland. https://gitlab.com/kicad/code/kicad/-/issues/14109[#14109]
