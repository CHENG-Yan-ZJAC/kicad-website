+++
title = "Avoid links to former kicad domain"
date = "2021-10-19"
draft = false
"blog/categories" = [
    "News"
]
+++

The original KiCad domain name (kicad-pcb.org) was recently sold to an unnamed third party that is not affiliated with the KiCad Project or members of the KiCad Development Team. This sale was unexpected and may pose a risk to KiCad users. The new owners may simply post advertisements or (worst-case scenario) they may host malicious versions of the KiCad software for download.

## How did this happen?
The domain name was originally registered in 2012 by the then project lead Dick Hollenbeck. As KiCad did not have a formal entity at the time, he registered it in his own name through his company SoftPLC. When the KiCad Project formally became a registered project under the Linux Foundation, we attempted to secure the rights to the original domain name from Dick without success.

So in 2020, link:https://www.digikey.com/en/blog/the-story-of-kicad-org[Digikey Corporation purchased the kicad.org domain name] from squatters and donated it to the KiCad Project, and we migrated to kicad.org back on October 20th, 2020. We migrated the majority of links in our control a year ago and link:https://www.kicad.org/blog/2020/10/kicad.org-the-permanent-internet-home-of-KiCad/[put up a notice], but we did not feel it was urgent to have all external resources updated at the time due to the past project relationship with the holder of the old domain.

## What is the KiCad Project doing now?
We are removing all links to the old domain name in the software for version 5.1.11 as well as 6.0. We are also contacting various sites that host content related to KiCad to update their links.

## What can I do?
Please do not contact Dick about this. He cannot undo the damage at this point. If you host KiCad videos on YouTube or content on a blog that has links to kicad-pcb.org, please update them to kicad.org as soon as possible. This will decrease the visibility of the old domain-name and limit the damage. You can also contact tech sites that post reviews of KiCad or otherwise have links to the old site and ask them to update their links.

TIP: When installing KiCad, please always verify the installation signature. Starting with KiCad version 5.99, all installs are signed by KiCad Services Corporation.

## What is KiCad doing to prevent this in the future?
The current kicad.org domain name is not under control of a single person. We have built in safeguards to our transfer process. Additionally, all KiCad trademarks are registered to the Linux Foundation for the sole purpose of advancing open-source EDA software.
