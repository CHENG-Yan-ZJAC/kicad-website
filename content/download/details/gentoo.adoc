+++
title = "Install on Gentoo"
summary = "Install instructions for KiCad on Gentoo"
+++
:dist: Gentoo

include::./content/download/_supported-note.adoc[]

== Stable Release

{{< repology gentoo >}}

KiCad is available in in the
link:https://packages.gentoo.org/packages/sci-electronics/kicad[official
gentoo packages]. You can install it via command line:

```
emerge sci-electronics/kicad
```
