+++
title = "KiCad Version 8 Release Candidate 1"
date = "2024-01-14"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is excited to announce the first release candidate
for the upcoming version 8 stable release.  Version 8 will contain
many new features compared to the current version 7 release.  Please
consider giving 8.0.0-rc1 a try to discover any additional issues so
they can be fixed before release at the end of January.  Thank you to
everyone who contributed to version 8.

## How to Download Release Candidate Builds

The release candidate packages can be found in the "Stable Builds"
folder on the https://www.kicad.org/download/[KiCad Download Page].

